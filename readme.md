# Objectif
Projet permettant de consolider les notions de programmation fonctionnel

J'ai essayé de réaliser un framework un peu comme flutter. 

L'application permet d'interagir avec trois chatbot avec des commandes simples. Par l'intermédiaire de ces commandes, un call api est fait.

Le parsage des données est ensuite transmis à l'IHM, de manière lisible par un humain. 

# Lancement de l'application

```bash
npm dev
```

