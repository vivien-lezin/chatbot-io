import './style.scss'
import {button, div, input, title} from "./framework";
import {Message, messagesComponent} from "./components";
import {Subject} from "rxjs";
import {allBots} from './bot';
import {botComponent} from './components/bot/bot.component';
import {messages, storageName} from "./storage";


(() => {
    function sendMessage(message: Message): void {
        onDisplayMessage$.next(message);
        messages.push(message);
        localStorage.setItem(storageName, JSON.stringify(messages));
    }

    const valueInput = {value: ""};
    const onSendUser$ = new Subject<Message>();
    const onDisplayMessage$ = new Subject<Message>();

    onSendUser$.subscribe(async (message: Message) => {
        let noFeature = true;
        sendMessage(message);
        //Logic bot
        for (const bot of allBots) {
            for (const feature of bot.features) {
                if (feature.check(message.text)) {
                    await new Promise<void>(async (resolve) => {
                        let messageToSend: Message;
                        if (feature.fetch) {
                            const data = await feature.fetch(message.text.split(":")[1].trim().toUpperCase());
                            messageToSend = {
                                ...feature.messageConstruct(data),
                            }
                        } else {
                            messageToSend = {
                                ...feature.messageConstruct(message.text.split(":")[1].trim().toUpperCase())
                            }
                        }
                        sendMessage({
                            ...messageToSend,
                            fromName: bot.name,
                            imageProfil: bot.image
                        });
                        noFeature = false;
                        resolve();

                    });
                }
            }
        }
        if (noFeature) {
            sendMessage({
                isFromUser: false,
                fromName: "None",
                date: new Date(),
                text: "No commands found. To have the available commands, try : </br><code>all:help</code>"
            });
        }
    });
    document.querySelector<HTMLDivElement>('#app')!.appendChild(
        div({
            className: "container-fluid",
            children: [
                div({
                    className: "row",
                    children: [
                        div({
                            className: "col-3 sidebar",
                            children: [
                                title({
                                    level: "h5",
                                    text: "List of bot",
                                }),//paragraph
                                div({
                                    className: "bot-list",
                                    children: allBots.map((bot) => botComponent(bot)),
                                })//div
                            ]
                        }),//div
                        div({
                            className: "col chat-container",
                            children: [
                                messagesComponent(onDisplayMessage$.asObservable()),//div chatmessages
                                div({
                                    className: "chat-input",
                                    children: [
                                        div({
                                            className: "input-group",
                                            children: [
                                                input({
                                                    className: "form-control",
                                                    placeholder: "Tapez votre message...",
                                                    type: "text",
                                                    value: valueInput,
                                                    onChange: (value: string) => {
                                                        valueInput.value = value;
                                                    }
                                                }),//input
                                                button({
                                                    type: 'Primary',
                                                    text: "Envoyer",
                                                    onClick: () => {
                                                        onSendUser$.next({
                                                            text: valueInput.value,
                                                            isFromUser: true,
                                                            date: new Date(),
                                                        });
                                                    },
                                                })//button
                                            ]
                                        }),//div
                                    ]
                                })//div
                            ]
                        })//div
                    ]
                })//div
            ]
        }))//div
    messages.forEach(m => {
        onDisplayMessage$.next(m)
    });
})()
