function getFormatedDate(date: Date): string {

    const padToTwoDigits = (num: number) => num.toString().padStart(2, '0');

    const day = padToTwoDigits(date.getDate());
    const month = padToTwoDigits(date.getMonth() + 1);
    const year = padToTwoDigits(date.getFullYear() % 100);


    const hours = padToTwoDigits(date.getHours());
    const minutes = padToTwoDigits(date.getMinutes());

    return `${day}/${month}/${year} - ${hours}:${minutes}`;
}

export default getFormatedDate;