function appendChildren(element: HTMLElement, children: HTMLElement[]): HTMLElement {
    if (children.length === 0) {
        return element;
    }
    element.appendChild(children[0]);
    children.shift();
    return appendChildren(element, children);
}

export default appendChildren;