import {Cryptocurrency, Musculation, WorldTime}  from './features';
import { Message } from "../components";
import checkHelp from './features/utils/check-help';

export type Feature = {
    check: (message: string) => boolean,
    fetch?: <T>(message: string) => Promise<T>,
    messageConstruct: <T>(response: T) => Message,
}

export type Bot = {
    features: Feature[]
    name: string,
    image: string
    description: string,
}


export const allBots: Bot[] = [
    {
        name: "CatTimeBot",
        image: "/istockphoto-1079767266-612x612.jpg",
        description: "Miawooo, I know time info like nobody",
        features: [
            {
                check: checkHelp,
                messageConstruct: WorldTime.Help.MessageConstruct
            },
            {
                check: WorldTime.IpInfo.Check,
                fetch: WorldTime.IpInfo.Fetch,
                messageConstruct: WorldTime.IpInfo.MessageConstruct,
            }
        ]
    },
    {
        name: "MuscleBot",
        image: "/big-strong-metal-muscle-gym-260nw-1697082349.webp",
        description: "For the go muscu",
        features: [
            {
                check: checkHelp, 
                messageConstruct: Musculation.Help.MessageConstruct
            },
            {
                check: Musculation.Exercices.Check,
                fetch: Musculation.Exercices.Fetch,
                messageConstruct: Musculation.Exercices.MessageConstruct
            },
            {
                check: Musculation.TypeExercices.Check,
                fetch: Musculation.TypeExercices.Fetch,
                messageConstruct: Musculation.TypeExercices.MessageConstruct
            }
        ]
    },
    {
        name: "CryptoBot",
        image: "/bitcoin-pixel-art.png",
        description: "He knows cryptocurrency like nobody",
        features: [
            {
                check:checkHelp,
                messageConstruct: Cryptocurrency.Help.MessageConstruct
            },
            {
                check: Cryptocurrency.Prices.Check,
                fetch: Cryptocurrency.Prices.Fetch,
                messageConstruct: Cryptocurrency.Prices.MessageConstruct
            }
        ]
    }
];