import {Message} from "../../../../components";
import {div, list, title} from "../../../../framework";

function messageConstruct(): Message {
    const elt = div({
            children: [
                title({
                    level: "h5",
                    text: "Command available for me"
                }),
                title({
                    level: "h6",
                    text: "Get time info from your IP"
                }),
                list({
                    type: "ul",
                    list: [
                        {
                            text: "time:ip"
                        }
                    ]
                })
            ]
        }
    )
    return {
        date: new Date(),
        isFromUser: false,
        text: elt.outerHTML,
    }
}


export default messageConstruct;