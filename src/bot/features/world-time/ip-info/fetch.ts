import {fetchUtils} from "../../utils/fetch-utils.ts";
import {baseUrl} from "../world-time.packet.ts";

export function fetch(): Promise<any> {
    return fetchUtils(`${baseUrl}ip`);
}