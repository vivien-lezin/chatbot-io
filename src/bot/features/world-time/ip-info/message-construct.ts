import {Message} from '../../../../components/';
import {div, paragraph, title} from "../../../../framework";
import {getFormatedDate} from "../../../../utils";

function messageConstruct(data: any): Message {
    const elt = div({
        children: [
            title({
                level: "h5",
                text: "Here time info from your IP address"
            }),
            paragraph({
                text: `Your IP adresse: <strong>${data.client_ip}</strong></br>
Your timezone: <strong>${data.timezone}</strong></br>
Date and hour: <strong>${getFormatedDate(new Date(data.datetime))}</strong>`
            }),
        ]
    })
    return {
        date: new Date(),
        text: elt.outerHTML,
        isFromUser: false
    }
}

export default messageConstruct;