import {checkUtils} from "../../utils/check-utils.ts";


function check(message: string): boolean{
    return checkUtils(["ip"], "time", message);
}

export default check;