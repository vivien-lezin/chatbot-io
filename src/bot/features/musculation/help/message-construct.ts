import { Message } from "../../../../components";
import { div, list, title } from "../../../../framework";
import { allMuscles } from "../exercices/fetch";
import { allTypes } from "../types/fetch";

function messageConstruct(): Message{
    const elt = div({
        children: [
            title({
                level: "h5",
                text: "Command available for me"
            }),
            title({
                level: "h6",
                text: "Get exercices from a provided muscle"
            }),
            list({
                type: "ul",
                list: allMuscles.map((muscle)=>({text:`muscle:${muscle}`}))
            }),
            title({
                level: "h6",
                text: "Get exercices from a type of exercice"
            }),
            list({
                type: "ul",
                list: allTypes.map((typeExercice)=>({text:`type:${typeExercice}`}))
            }),
        ]
    });
    return {
        date: new Date(),
        isFromUser: false,
        text: elt.outerHTML,
    }
}

export default messageConstruct;