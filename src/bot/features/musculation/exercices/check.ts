import {allMuscles} from './fetch.ts';
import {checkUtils}from '../../utils/check-utils.ts';

function check(message: string) {
    return checkUtils(allMuscles,"muscle",message);
};

export default check;