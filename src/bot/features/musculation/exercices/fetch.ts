import {baseUrl, apiKey} from '../musculation.packet';
import {fetchUtils} from '../../utils/fetch-utils';

export const allMuscles = ["abdominals", "abductors", "adductors", "biceps", "calves", "chest", "forearms", "glutes", "hamstrings", "lats", "lower_back", "middle_back", "neck", "quadriceps", "traps", "triceps"]

type SuitMuscles = typeof allMuscles;
export type Muscle = SuitMuscles[number];


export async function fetch(muscle: Muscle): Promise<any> {
    return fetchUtils(`${baseUrl}exercises`,{
        muscle: muscle
    },{
        ...apiKey
    });
}

