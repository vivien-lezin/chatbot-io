import { Message } from "../../../../components";
import messageConstructGeneral from '../message-construct-general';

function messageConstruct(data: any): Message {
    return messageConstructGeneral(data, data[0].muscle);
}

export default messageConstruct;