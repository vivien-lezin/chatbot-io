import { Message } from "../../../components";
import { list } from "../../../framework";

function messageConstructGeneral(data: any, dataFrom: string): Message {
    if (data.length === 0){
        return {
            text: `No data from this command`,
            date: new Date(),
            isFromUser: false,
            fromName: "MuscleBot"
        }
    }
    const listElt = list({
        list: data.map((exercice: {name: string}) => {
            return {
                text: exercice.name
            }
        }),
         type: "ul"
    });
    return {
        text: `<h5>Here the list of exercices for ${dataFrom}</h5> ${listElt.outerHTML}`,
        date: new Date(),
        isFromUser: false,
    }
};

export default messageConstructGeneral;