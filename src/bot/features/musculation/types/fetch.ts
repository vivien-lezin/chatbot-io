import { baseUrl, apiKey } from '../musculation.packet';
import {fetchUtils} from '../../utils/fetch-utils';

export const allTypes = [
    "cardio",
    "olympic_weightlifting",
    "plyometrics",
    "powerlifting",
    "strength",
    "stretching",
    "strongman"
];


type SuitTypes = typeof allTypes;
export type TypeExercice = SuitTypes[number];

async function fetch(typeExercice: TypeExercice): Promise<any>{
    return fetchUtils(`${baseUrl}exercises`,{
        type: typeExercice
    },{
    ...apiKey
    });
}

export default fetch;