import { allTypes } from "./fetch";
import {checkUtils}from '../../utils/check-utils.ts';

function check(message: string): boolean{
    return checkUtils(allTypes,"type",message);
}

export default check;