import {apiKey as apiKeyGeneral, baseUrl as baseUrlGeneral} from "../musculation/musculation.packet";

export const baseUrl = baseUrlGeneral;
export const apiKey = apiKeyGeneral;