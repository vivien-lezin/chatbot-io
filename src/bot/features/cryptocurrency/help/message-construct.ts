import { Message } from "../../../../components";
import { div, list, title } from "../../../../framework";
import { allSymbols } from "../prices/fetch";

function messageConstruct(): Message{
    const elt = div({
        children: [
            title({
                level: "h5",
                text: "Command available for me"
            }),
            title({
                level: "h6",
                text: "Get price from a provided symbol"
            }),
            list({
                type: "ul",
                list: allSymbols.map((symbol)=>({text:`crypto:${symbol}`}))
            })
        ]
    });
    return {
        date: new Date(),
        isFromUser: false,
        text: elt.outerHTML,
    }
}

export default messageConstruct;