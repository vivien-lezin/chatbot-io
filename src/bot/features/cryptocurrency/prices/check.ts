import { checkUtils } from "../../utils/check-utils";
import { allSymbols } from "./fetch";

export function check(message: string): boolean{
    return checkUtils(allSymbols,"crypto",message);
}