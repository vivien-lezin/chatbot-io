import { Message } from "../../../../components";

export function messageConstruct(data: any): Message{
    console.log(data);
    
    return {
        text: `The price for "${data.symbol}" is ${data.price}.`,
        date: new Date(),
        isFromUser: false,
    }
}