import { checkUtils } from "./check-utils";

function checkHelp(message: string){
    return checkUtils(["help"], "all", message);
};

export default checkHelp;