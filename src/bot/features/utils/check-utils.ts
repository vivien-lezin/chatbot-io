export function checkUtils(collection:any[], firstWordToCheck: string , message: string): boolean{
    const words = message.split(":");
    if (words[0].trim().toUpperCase() !== firstWordToCheck.toUpperCase()){
        return false;
    }
    return collection.findIndex((word: string) => word.toUpperCase() === words[1].trim().toUpperCase()) !== -1;
}