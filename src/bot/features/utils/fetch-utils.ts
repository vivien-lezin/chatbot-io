import get from "axios";

export async function fetchUtils(baseUrl: string, params?: {[key:string]: any}, headers?: {[key:string]: string}): Promise<any>{
    return (await get(`${baseUrl}`, {
        params,
        headers: {
            ...headers
        }
    })).data;
}