import {createHtmlElement, CreateHtmlElementParameters} from "./main.ts";

export type CreateHTMLpElementParameters = CreateHtmlElementParameters &
    Omit<CreateHtmlElementParameters, "text"> &
    {
        text: string;
    }


export function paragraph(parameters: CreateHTMLpElementParameters): HTMLParagraphElement{
    const pElt = createHtmlElement("p", {
        ...parameters
    }) as HTMLParagraphElement;

    return pElt;
}