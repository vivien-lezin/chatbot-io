import {createHtmlElement} from "./main.ts";
import {CreateHTMLpElementParameters} from "./paragraph.ts";

type CreateHTMLAElementParameters = CreateHTMLpElementParameters &
    { href: string;}

export function link(parameters: CreateHTMLAElementParameters){
    const aElt = createHtmlElement("a", {...parameters}) as HTMLLinkElement
    aElt.href = parameters.href;
    return aElt;
}