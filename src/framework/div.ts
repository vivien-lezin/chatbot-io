import {createHtmlElement, CreateHtmlElementParameters} from "./main.ts";

export function div(parameters?: CreateHtmlElementParameters): HTMLDivElement{
    const divElt = createHtmlElement('div', {...parameters}) as HTMLDivElement;

    return divElt;
}