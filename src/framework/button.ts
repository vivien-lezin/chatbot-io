import {createHtmlElement, CreateHtmlElementParameters} from "./main.ts";

type typeButton = "Primary" | "Secondary" | "Success" | "Danger";

type CreateButtonElementParameters = CreateHtmlElementParameters
    & Omit<CreateHtmlElementParameters, "onClick">
    & Omit<CreateHtmlElementParameters, "htmlTag">
    & { onClick: Function }
    & { type: typeButton }

function getClassNameFromType(type: typeButton): string {
    switch (type) {
        case "Primary":
            return "btn-primary";
        case "Secondary":
            return "btn-secondary";
        case "Success":
            return "btn-success";
        case "Danger":
            return "btn-danger";
        default:
            console.warn(`The className "${type}" wasn't found in getClassNameFromType function`);
            return "";
    }
}

export function button(parameters: CreateButtonElementParameters): HTMLButtonElement {
    const buttonElt = createHtmlElement("button",{
        ...parameters,
    }) as HTMLButtonElement;
    buttonElt.className += " btn " + getClassNameFromType(parameters.type);
    return buttonElt;

}