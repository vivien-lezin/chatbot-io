import {createHtmlElement, CreateHtmlElementParameters} from "./main.ts";

type CreateInputHtmlElementParameters = CreateHtmlElementParameters &
    {
        placeholder?: string,
        value?: { value: string },
        type: "text" | "password" | "email" | "number" | "file" | "hidden",
        onChange?: (value: string) => void,
    };


export function input(parameters: CreateInputHtmlElementParameters): HTMLInputElement {
    const inputElt = createHtmlElement("input", {...parameters}) as HTMLInputElement;
    if (parameters.onChange) {
        inputElt.addEventListener("change", () => {
            parameters.onChange!(inputElt.value);
        });
    }
    return inputElt;
}
