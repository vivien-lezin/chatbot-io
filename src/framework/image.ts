import { CreateHtmlElementParameters, createHtmlElement } from "./main";

type CreateImageElementParameters = CreateHtmlElementParameters & 
{
    src: string
    alt?: string
}

function image(parameters: CreateImageElementParameters):HTMLImageElement{
    const imageElt = createHtmlElement("img", {
        ...parameters
    }) as HTMLImageElement;

    imageElt.src = parameters.src;
    if (parameters.alt){
        imageElt.alt = parameters.alt;
    }

    return imageElt;
}

export default image;