import {createHtmlElement, CreateHtmlElementParameters} from "./main.ts";

type CreateTitleElementParameters = CreateHtmlElementParameters &
    {level: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';}
export function title(parameters: CreateTitleElementParameters): HTMLHeadingElement {
    const titleElement = createHtmlElement(parameters.level, {...parameters}) as HTMLHeadingElement;
    return titleElement;
}