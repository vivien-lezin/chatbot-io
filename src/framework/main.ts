import {appendChildren} from '../utils'

export type CreateHtmlElementParameters = {
    className?: string,
    onClick?: ()=>void,
    text?: string,
    children?: Array<HTMLElement>,
}

export function createHtmlElement(htmlTag: keyof HTMLElementTagNameMap,parameters: CreateHtmlElementParameters): HTMLElement {
    const elt = document.createElement(htmlTag);
    if (parameters.onClick){
        elt.addEventListener("click", parameters.onClick);
    }
    if (parameters.text){
        elt.innerHTML = parameters.text;
    }
    if (parameters.className) {
        elt.className = parameters.className;
    }
    if (parameters.children){
        appendChildren(elt, parameters.children);
    }
    return elt;
}