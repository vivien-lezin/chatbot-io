import {createHtmlElement, CreateHtmlElementParameters} from "./main.ts";

type CreateHtmlUlListElementParameters = CreateHtmlElementParameters & {
    list: CreateHtmlElementParameters[];
    type: "ul" | "ol"
} & {type: "ul"}

export function list(parameters: CreateHtmlUlListElementParameters): HTMLUListElement{
    const ulElt = createHtmlElement("ul", parameters) as HTMLUListElement;
    parameters.list.forEach((param) => {
        ulElt.appendChild(createHtmlElement("li", param) as HTMLLIElement);
    });
    return ulElt;
}