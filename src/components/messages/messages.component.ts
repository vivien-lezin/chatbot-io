import "./messages.component.scss";
import {div} from "../../framework";
import {Observable} from "rxjs";
import {getFormatedDate} from "../../utils";
import image from "../../framework/image";

export type Message = {
    fromName?: string;
    date: Date;
    isFromUser: boolean;
    text: string;
    imageProfil?: string;
}

function onSubscribeMessage(message: Message, elt: HTMLElement): void {
    const children = [];
    if (message.imageProfil){
        children.push(image({
            src:message.imageProfil
        }));
    }
    children.push(div({
        className: `message`,
        children: [
            div({
                text: `<small><strong>${message.isFromUser ? 'You' : message.fromName}</strong></small> </br> ${message.text}`,
            }),//div
            div({
                className: "message-date",
                text: `${getFormatedDate(message.date)}`
            })//div
        ]
    }))
    const messageElt = div({
        className: `message-container ${message.isFromUser ? "user" : "bot"}`,
        children
    });
    
    elt.appendChild(messageElt);
    elt.scrollTop = elt.scrollHeight;
}

export function messagesComponent(onDisplayMessage$: Observable<Message>): HTMLDivElement {
    let elt = div({
        className: "chat-messages",
    });
    onDisplayMessage$.subscribe((message: Message) => onSubscribeMessage(message, elt));
    return elt;
}