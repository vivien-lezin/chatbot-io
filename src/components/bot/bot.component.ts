import './bot.component.scss';
import {Bot} from '../../bot';
import {div, paragraph} from '../../framework';
import image from '../../framework/image';

export function botComponent(bot: Bot): HTMLDivElement {
    return div({
        className: "bot-container",
        children: [
            image({
                src: bot.image
            }),
            div({
                className: "bot-text",
                children: [
                    paragraph({
                        text: bot.name
                    }),
                    paragraph({
                        text: bot.description
                    })
                ]
            }),
        ]
    })
}
