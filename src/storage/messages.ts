import {Message} from "../components";
import initMessages from "./function/init-messages.ts";

export const storageName = 'messageHistory';
export const messages: Message[] =  initMessages();