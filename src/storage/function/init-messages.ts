import {Message} from "../../components";
import {storageName} from "../messages";

function initMessages(): Message[] {
    return JSON.parse(localStorage.getItem(storageName) ?? '[]')
        .map((m: Message) => ({
            ...m,
            date: new Date(m.date),
        }))
}

export default initMessages;